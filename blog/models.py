from django.db import models
from django.urls import reverse


class Post(models.Model):
    title = models.CharField(max_length=200)
    # many to one relationship
    author = models.ForeignKey(
        'auth.User',
        on_delete=models.CASCADE,
    )
    body = models.TextField()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        # reference to object by it's URL template name ("pk" and "id")
        # are interchangeable like "object" and "post" and so on
        """Django docs recommend using self.id with get_absolute_url. So we’re
telling Django that the ultimate location of a Post entry is its post_detail view which
is posts/<int:pk>/ so the route for the first entry we’ve made will be at posts/1."""
        return reverse('post_detail', args=[str(self.id)])
