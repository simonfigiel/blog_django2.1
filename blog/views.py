from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.views.generic.edit import (
                                        CreateView,
                                        UpdateView,
                                        DeleteView,
                                    )
from django.urls import reverse_lazy

from .models import Post


class BlogListView(ListView):
    model = Post
    template_name = 'home.html'


class BlogDetailView(DetailView):
    model = Post
    template_name = 'post_detail.html'
    # context_object_name = 'some_name'it refers to objects name in
    # our template {{some_name.title}}


class BlogCreateView(CreateView):
    # database model Post
    model = Post
    template_name = 'post_new.html'
    # simply take all fields
    fields = '__all__'

class BlogUpdateView(UpdateView):
    model = Post
    template_name = 'post_edit.html'
    fields = ['title', 'body']


class BlogDeleteView(DeleteView):
    model = Post
    template_name = 'post_delete.html'
    success_url = reverse_lazy('home')
    """We use reverse_lazy as opposed to just reverse so that it
     won’t execute the URL redirect until our view has finished
      deleting the blog post."""